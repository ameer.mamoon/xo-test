// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/xo.dart';

void main() {
  runApp( MyApp());
}

class MyApp extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    // return MaterialApp(
    //   home:Scaffold(
    //     body: Container(
    //       width: double.infinity,
    //       height: double.infinity,
    //       decoration: BoxDecoration(
    //         gradient: LinearGradient(
    //           colors: [Colors.blue,Colors.lightBlueAccent],
    //           begin: Alignment.topLeft,
    //           end: Alignment.bottomRight
    //         ),
    //       ),
    //       child: SafeArea(
    //           child: Column(
    //             mainAxisAlignment: MainAxisAlignment.center,
    //             children: [
    //               CircleAvatar(
    //                 radius: 50,
    //                 foregroundImage: AssetImage('images/w.jpg'),
    //               ),
    //               SizedBox(height: 10,),
    //               Text('Ameer',
    //               style: TextStyle(
    //                 color: Colors.white,
    //                 fontSize: 32,
    //                 fontFamily: 'BhuTukaExpandedOne'
    //               ),
    //               ),
    //               SizedBox(height: 10,),
    //               Text('Full Stack dev',
    //                 style: TextStyle(
    //                     color: Colors.white,
    //                     fontSize: 24,
    //                     fontFamily: 'BhuTukaExpandedOne'
    //                 ),
    //               ),
    //               info(Icon(Icons.phone,color:Colors.blue),'093480138'),
    //               info(Icon(Icons.email,color:Colors.blue),'ameer@gmail.com'),
    //               info(Icon(Icons.location_on,color:Colors.blue), 'sy')
    //             ],
    //           )
    //       )
    //     ),
    //   )
    // );
    return MaterialApp(
      home: Home(),
    );
  }


}

// class info extends StatelessWidget {
//   final Widget icon;
//   final String text;
//
//
//   info(this.icon, this.text);
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       width: 310,
//       height: 50,
//       decoration: BoxDecoration(
//           color: Colors.white,
//           borderRadius: BorderRadius.circular(5)
//       ),
//       padding: EdgeInsets.all(10),
//       margin: EdgeInsets.all(10),
//       child: Row(
//         children: [
//           icon,
//           SizedBox(width: 10,),
//           Text(text,style: TextStyle(
//             color: Colors.blueGrey,
//           ),)
//         ],
//       ),
//     );
//   }
// }
