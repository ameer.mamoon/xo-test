// ignore_for_file: prefer_const_constructors

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/xoLogic.dart';

xoLogic xo = xoLogic();

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Xo Game'),
        centerTitle: true,
      ),
      body: xoBody(),
    );
  }
}

class xoBody extends StatefulWidget {
  @override
  State<xoBody> createState() => _xoBodyState();
}

class _xoBodyState extends State<xoBody> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        for(int j = 0 ; j < 3 ; j++)
        xoRow(j),
        SizedBox(
          height: 20,
        ),
        IconButton(onPressed: (){
          setState(() {
            xo = xoLogic();
          });
        },
            icon: Icon(Icons.replay,color: Colors.blue,size: 60,))
      ],
    );
  }
}

class xoRow extends StatelessWidget {
  final int j;

  xoRow(this.j);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        for(int i = 0 ; i < 3 ; i++)
        xoWidget(i,j),

      ],
    );
  }
}


class xoWidget extends StatefulWidget {
  final int i;
  final int j;
  String val = '';

  xoWidget(this.i, this.j);

  @override
  State<xoWidget> createState() => _xoWidgetState();
}

class _xoWidgetState extends State<xoWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        setState(() {
          if(widget.val.isEmpty){
          widget.val = (xo.play(widget.i,widget.j)) ? 'X':'O';
          xo.check();
          }
        });
      },
      child: Container(
        width: 100,
        height: 150,
        color: Colors.grey,
        margin: EdgeInsets.all(10),
        child: Text(widget.val),
        alignment: Alignment.center,
      ),
    );
  }
}
