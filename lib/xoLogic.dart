
// ignore_for_file: curly_braces_in_flow_control_structures

class xoLogic{
  bool isX = true;
  List<List<bool?>> grid = [
    for(int i = 0;i <3;i++)
      [null,null,null]
  ];

  bool play(int i,int j){
    grid[i][j] = isX;
    isX = !isX;
    return grid[i][j]!;
  }

  String check() {
    for (int i = 0; i < 3; i++)
      if (grid[i][0] == grid[i][1] && grid[i][2] == grid[i][1] &&
          grid[i][0] != null) {
        return grid[i][1]! ? 'X win' : 'O win';
      }
    for (int i = 0; i < 3; i++)
      if (grid[0][i] == grid[1][i] && grid[2][i] == grid[1][i] &&
          grid[0][i] != null) {
        return grid[0][i]! ? 'X win' : 'O win';
      }

    if (grid[0][0] == grid[1][1] && grid[2][2] == grid[1][1] &&
        grid[0][0] != null) {
      return grid[0][0]! ? 'X win' : 'O win';
    }

    if (grid[0][2] == grid[1][1] && grid[2][0] == grid[1][1] &&
        grid[1][1] != null) {

      return grid[1][1]! ? 'X win' : 'O win';
    }

    for(int i = 0 ; i < 3 ; i++)
      for(int j = 0 ; j < 3 ; j++)
        if(grid[i][j] == null){
          return 'not done yet';
        }
    return 'Draw';
  }
}